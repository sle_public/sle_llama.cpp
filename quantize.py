
# Variables
MODEL_ID = "silverliningeda/llama-2-7b-silverliningeda-verilog-codegen"
QUANTIZATION_METHODS = ["q4_k_m"]

# Constants
MODEL_NAME = MODEL_ID.split('/')[-1]
GGML_VERSION = "gguf"

# Install llama.cpp
!git clone https://github.com/ggerganov/llama.cpp
!cd llama.cpp && git pull && make clean && LLAMA_CUBLAS=1 make
!pip install -r llama.cpp/requirements.txt

# Download model
!git lfs install
!git clone https://huggingface.co/{MODEL_ID}

# Convert to fp16
fp16 = f"{MODEL_NAME}/{MODEL_NAME.lower()}.{GGML_VERSION}.fp16.bin"
!python llama.cpp/convert.py {MODEL_NAME} --outtype f16 --outfile {fp16}

# Quantize the model for each method in the QUANTIZATION_METHODS list
for method in QUANTIZATION_METHODS:
    qtype = f"{MODEL_NAME}/{MODEL_NAME.lower()}.{GGML_VERSION}.{method}.bin"
    !./llama.cpp/quantize {fp16} {qtype} {method}

## Run inference

Here is a simple script to run your quantized models. I'm offloading every layer to the GPU (35 for a 7b parameter model) to speed up inference.

import os

model_list = [file for file in os.listdir(MODEL_NAME) if GGML_VERSION in file]

prompt = input("Enter your prompt: ")
chosen_method = input("Please specify the quantization method to run the model (options: " + ", ".join(model_list) + "): ")

# Verify the chosen method is in the list
if chosen_method not in model_list:
    print("Invalid method chosen!")
else:
    qtype = f"{MODEL_NAME}/{MODEL_NAME.lower()}.{GGML_VERSION}.{method}.bin"
    !./llama.cpp/main -m {qtype} -n 128 --color -ngl 35 -p "{prompt}"

## Push to hub

To push your model to the hub, run the following blocks. It will create a new repo with the "-GGUF" suffix. Don't forget to change the `username` variable in the following block.

!pip install -q huggingface_hub

username = "silverliningeda"

from huggingface_hub import notebook_login, create_repo, HfApi
notebook_login()

api = HfApi()


create_repo(repo_id = f"{username}/{MODEL_NAME}-GGUF", repo_type="model", exist_ok=True)

api.upload_folder(
    folder_path=MODEL_NAME,
    repo_id=f"{username}/{MODEL_NAME}-GGUF",
    allow_patterns=f"*{GGML_VERSION}*",
)
