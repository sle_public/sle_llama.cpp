!pip install -q datasets
!huggingface-cli login

from datasets import load_dataset
import re

# Load the dataset
dataset = load_dataset('timdettmers/openassistant-guanaco')

# Shuffle the dataset and slice it
dataset = dataset['train'].shuffle(seed=42).select(range(500))

module_descriptor = ["generate verilog for a 8 bit counter"]
code_listing      = ["""
module 8bit_counter (
output logic [7:0] out,
input logic enable,
input logic clk,
input logic reset
);
always @(posedge clk)
if (reset) begin
  out <= 8'b0 ;
end else if (enable) begin
  out <= out + 1;
end
endmodule"""]
 
def transform_conversation(example):
   

    reformatted_segments = []

    # Iterate over pairs of segments
    print (len(module_descriptor))
    #for i in range(0, len(module_descriptor)):
    human_text = module_descriptor[0]
    #module_descriptor.pop(0)

    print(human_text)

    assistant_text = code_listing[0]
    #code_listing.pop(0)
        
    print (assistant_text)

        # Apply the new template
    reformatted_segments.append(f'<s>[INST] {human_text} [/INST] {assistant_text} </s>')
        

    return {'text': ''.join(reformatted_segments)}


# Apply the transformation
transformed_dataset = dataset.map(transform_conversation)


transformed_dataset.push_to_hub("silverliningeda-dataset-test")
